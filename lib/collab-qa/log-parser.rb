require 'liquid'
require 'collab-qa/log-parser-build'
require 'collab-qa/log-parser-piuparts'
require 'collab-qa/log-parser-instest'
require 'collab-qa/encoding'

module Enumerable
  def grep_index(re)
    ind = []
    each_with_index do |e,i|
      if re === e
        ind << i
      end
    end
    return ind
  end
end

module CollabQA
  class Log
    attr_reader :file, :data, :lines, :package, :version, :result, :time, :reasons, :sum_1l, :sum_ml, :extract, :logtype, :comment

    def initialize(file)
      @file = file
      @data = IO::read(@file).strip

      #
      # Encoding crap
      #
      # Ruby string.encode! is not working as expected: it didn't check for
      # valid byte sequences:
      #
      #   - Oniguruma didn't warn about this bad sequences:
      #
      #       From oniguruma docs:
      #         A-6. Problems
      #
      #           + Invalid encoding byte sequence is not checked.
      #
      #             ex. UTF-8
      #
      #             * Invalid first byte is treated as a character.
      #               /./u =~ "\xa3"
      #
      #             * Incomplete byte sequence is not checked.
      #               /\w+/ =~ "a\xf3\x8ec"
      #
      #   - Running RE2 on this recoded lines, warns about positional errors:
      #
      #       - ""RE2: invalid startpos, endpos pair""
      #
      begin
        # force encoding.
        #if @data.respond_to?(:encode!)
        #  @data.encode!('UTF-16', 'UTF-8', :invalid => :replace, :replace => '')
        #  @data.encode!('UTF-8', 'UTF-16')
        #end
        @lines = @data.split(/\n/)
      rescue
        # Read lines again and remove the invalid ones
        @lines = IO::readlines(@file)
        @lines.map! { |l| to_valid_utf8(l.chomp!)  }
      end
      @lines.map! { |l| l.gsub("\0", "") } # remove null characters

      @reasons, @sum_1l, @sum_ml, @extract = nil
      @logtype = nil
      @package, @version, @result, @time, @buildtime = 'UNKNOWN'
      @comment = nil

      dbh = @lines.grep(/^DC-Build-Header:/)[0]
      if dbh
        _, @package, @version, _ = dbh.split(' ', 4)
        @logtype = :build
        dbs = @lines.grep(/^DC-(Build-)?Status:/)[0]
        if dbs
          _, @result, @time = dbs.split(' ', 3)
          @time = @time.to_f
        else
          @result = "Unknown"
          @time = 0
        end
        # handle retries, remove first log
        reti = @lines.grep_index(/DC-Message: Failed, but took only.*Retrying, you never know./)[-1]
        if reti
          @lines = @lines[0..0] + @lines[(reti+1)..-1]
        end
        dbn = @lines.grep(/^Build needed/)[0]
        if dbn
          t, _ = dbn.split(' ',4)
          @buildtime = t.split(/:|,/).map { |i| i.to_i }.inject(0) { |a, b| a * 60 + b }
        end
      elsif @lines[0] =~ /^sbuild/
        # plain sbuild logs
        @logtype = :build
        @package = @lines.grep(/^Package: /)[0].split[1]
        @version = @lines.grep(/^Version: /)[0].split[1]
        @result = @lines.grep(/^Status: /)[-1].split[1]
        @result = "OK" if @result == "successful"
        @time = @lines.grep(/^Build-Time: /)[0].split[1].to_f
      elsif dph = @lines.grep(/^DC-Piuparts-Header:/)[0]
        @logtype = :piuparts
        _, @package, _ = dph.split(' ', 4)
        # guess the version from the log. yeah, tricky.
        @version = 'UKN'
        @lines.each do |l|
         if (m = l.match(/ Setting up (.*) \((.*)\).../))
          next if m[1] != @package
          @version = m[2]
         end
        end
        dps = @lines.grep(/^DC-Piuparts-Status:/)[0]
        if dps
         _, @result, @time = dps.split(' ', 3)
         @time = @time.to_f
        else
         @result = "Unknown"
         @time = 0
        end
      elsif dph = @lines.grep(/^IT-Header:/)[0]
        @logtype = :instest
        _, @package, _ = dph.split(' ', 4)
        ver = @lines.grep(/^-- Finding version: /)[0]
        if ver
          @version = ver.split(' ')[4]
        else
          @version = 'UNKNOWN'
        end
        dps = @lines.grep(/^-- Result: /)[0]
        if dps
          @result = dps.split(' ')[2]
          @time = @lines.grep(/^-- Total time: /)[4].to_f
        else
          @result = "Unknown"
          @time = 0
        end
      end
    end

    def match_one_amongst?(line, regexps)
      regexps.each do |e|
        return true if line =~ e
      end
      return false
    end

    def guess_failed
      @reasons = []
      return if @result == "OK"
      if @logtype == :build
        guess_failed_build
      elsif @logtype == :piuparts
        guess_failed_piuparts
      elsif @logtype == :instest
        guess_failed_instest
      end
    end

    def extract_log
      return if @result == 'OK'
      guess_failed if @reasons.nil?
      if @logtype == :build
        extract_log_build
      elsif @logtype == :piuparts
        extract_log_piuparts
      elsif @logtype == :instest
        extract_log_instest
      end
    end

    def reasons_to_s
      if @reasons.nil?
        return "REASONS_NOT_COMPUTED"
      elsif @reasons.empty?
        return "UNKNOWN"
      else
        return "#{@reasons.join('/')}"
      end
    end

    def oneline_to_s(disptime = false)
      if disptime
        return "#{@package} #{@version} #{@result} #{@time} #{@buildtime} [#{reasons_to_s}] #{@sum_1l}\n"
      else
        return "#{@package} #{@version} #{@result} [#{reasons_to_s}] #{@sum_1l}\n"
      end
    end

    def to_s
      s = "#{@package} #{@version} #{@result} #{@time}\n"
      s += "Reason: #{reasons_to_s}\n"
      if @extract
        if @sum_1l
          s += "\n1-line summary:\n"
          s += "#{@sum_1l}\n"
        end
        if @sum_ml
          s += "\nmulti-line summary (lines: #{@sum_ml.length}):\n"
          s += "#{@sum_ml.join("\n")}\n"
        end
        s += "\nexcerpt for mail (lines: #{@extract.length}):\n"
        s += "#{@extract.join("\n")}\n"
      end
      s
    end

    # https://snipplr.com/view/1252/rails-breakup-long-lines
    def break_up_long_line(str, max)
      counter = 0
      new_text = ''
      for i in 0...str.length
        if str[i] == "\n"
          counter = 0
        else
          counter = counter + 1
        end
        new_text << str[i,1]
        if counter >= max && str[i] =~ /\s/
          new_text << "\n"
          counter = 0
        end
      end
      new_text
    end

    def extract_split_long_lines
      @extract = break_up_long_line(@extract.join("\n"), 800).split("\n")
    end

    def to_mail_with_template(date, fullname, email, template_file, filename = "")
      template = Liquid::Template.parse(File.read(template_file))
      template.render({
        'date' => date,
        'date_without_slashes' => date.gsub(/\//, ''),
        'fullname' => fullname,
        'email' => email,
        'package' => self.package,
        'version' => self.version,
        'summary' => self.sum_1l,
        'extract' => self.extract,
        'filename' => filename,
      })
    end

    def to_mail(date, fullname, email, bugtype, filename = "")
      bugtype ||= "ftbfs"
      template = File.join(File.dirname(__FILE__), "bug_templates", "#{bugtype}.txt.liquid")
      to_mail_with_template(date, fullname, email, template, filename)
    end
  end
end
