basedir=$(readlink -f $(dirname "$BASH_SOURCE"))
echo $basedir

export RUBYLIB=${basedir}/lib
export PATH=${basedir}/bin:${PATH}

# cleanup
unset basedir

